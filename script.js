/* TODO:
* Programatically calculate sizes for door and floor, apply
* Determine correct gradient bg for close up view
* DONE:
* Grid of click listeners to allow navigation
* Track position in hall
* 2 Dimensional array of art in POSITIONS
*
*/
const img0 = 'https://media.giphy.com/media/3oFyCZm9jPjlzsAwXS/source.gif'
const img1 = 'https://culturepowerpolitics.files.wordpress.com/2018/03/glitch-art-photography-15.jpg?w=363&h=242'
const img2 = 'https://i.pinimg.com/236x/71/b9/8c/71b98c1a74f78ec78379d2bdb0f7cf04--dog-photos-the-inspiration.jpg'
const img3 = 'https://kerrynourricephotography.weebly.com/uploads/1/3/8/4/13847016/7461297_orig.jpg'
const img4 = 'https://www.themeraider.com/wp-content/uploads/2016/11/glitch-art-background-17.jpg'
const img5 = 'https://i.pinimg.com/736x/28/b3/f4/28b3f4b96d5b848ff485166adff520e5--modern-photography-color-photography.jpg'
const img6 = 'https://78.media.tumblr.com/f4bddf353612d6983faeb1efb7a2d79c/tumblr_pdnw908Xsh1uxzaugo1_500.gif'
const img7 = 'https://zfresh.files.wordpress.com/2015/01/glitch.png'

const POSITIONS = [
  {name: 'alfa', art1: img1,  art2: img1},
  {name: 'bravo', art1: img2,  art2: img2},
  {name: 'charlie', art1: img3,  art2: img3},
  {name: 'delta', art1: img4, art2: img4},
  {name: 'echo', art1: img5, art2: img5},
  {name: 'foxtrot', art1: img6, art2: img6},
  {name: 'golf', art1: img7, art2: img7},
  {name: 'hero', art1: img0, art2: img0}
]
const hallway = document.querySelector('#hallway')
const wall = document.querySelector('#closeup')

window.addEventListener('load', (event) => {
  let INDEX = 0
  hangWallArt(POSITIONS[INDEX])

  document.querySelector('#away').addEventListener('click', (e) =>{INDEX = moveZer(INDEX) })
  document.querySelector('#footboardaway1').addEventListener('click', (e) =>{INDEX = moveZer(INDEX) })
  document.querySelector('#footboardaway2').addEventListener('click', (e) =>{INDEX = moveZer(INDEX) })
  document.querySelector('#walkway').addEventListener('click', (e) =>{INDEX = moveZer(INDEX) })
  document.querySelector('#footboardback1').addEventListener('click', (e) =>{INDEX = moveAer(INDEX) })
  document.querySelector('#footboardback2').addEventListener('click', (e) =>{INDEX = moveAer(INDEX) })
  document.querySelector('#art1').addEventListener('click', (e)=>{viewArt(POSITIONS[INDEX])})
  document.querySelector('#art2').addEventListener('click', (e)=>{viewArt(POSITIONS[INDEX])})
  closeup.addEventListener('click', (e)=>{exitArt(POSITIONS[INDEX])})
});

function moveZer(index){
  let newIndex = index + 1
  if(newIndex >= POSITIONS.length){newIndex = 0}
  hangWallArt(POSITIONS[newIndex], true)
  hallway.className = ""
  hallway.classList.add(`pos${newIndex}`)
  return newIndex
}

function moveAer(index){
  let newIndex = index - 1
  if(newIndex < 0){newIndex = POSITIONS.length - 1}
  hangWallArt(POSITIONS[newIndex], false)
  hallway.className = ""
  hallway.classList.add(`pos${newIndex}`)
  return newIndex
}

function hangWallArt(currPosi, fwd = true){
  console.log(`hanging art for position ${currPosi.name}`)
  let animation = fwd ? 'come-in' : 'come-out'
  let art1 = document.createElement('img')
  art1.src = currPosi.art1
  art1.classList.add(animation)

  let art2 = document.createElement('img')
  art2.src = currPosi.art2
  art2.classList.add(animation)

  let wall1 = document.querySelector('#art1')
  wall1.innerHTML = ''
  wall1.appendChild(art1)

  let wall2 = document.querySelector('#art2')
  wall2.innerHTML = ''
  wall2.appendChild(art2)
}

function viewArt(currPosi){
  hallway.classList.add('hidden')
  wall.classList.remove('hidden')
  let art1 = document.createElement('img')
  art1.src = currPosi.art1
  let closeup = document.querySelector("#art1-close")
  closeup.innerHTML = ''
  closeup.appendChild(art1)
}

function exitArt(currPosi){
  wall.classList.add('hidden')
  hallway.classList.remove('hidden')
}
